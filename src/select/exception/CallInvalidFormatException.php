<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\select\exception;

use liberty_code\call\call\api\CallInterface;
use liberty_code\call\select\library\ConstSelector;



class CallInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $call
     */
	public function __construct($call)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstSelector::EXCEPT_MSG_CALL_INVALID_FORMAT,
            mb_strimwidth(strval($call), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified list of calls has valid format.
     *
     * @param mixed $tabCall
     * @return boolean
     */
    protected static function checkTabCallIsValid($tabCall)
    {
        // Init var
        $result = is_array($tabCall);
        $tabClassPath = array();

        // Check each call is valid
        for($intCpt = 0; ($intCpt < count($tabCall)) && $result; $intCpt++)
        {
            $call = $tabCall[$intCpt];
            $result = (
                // Check valid call
                ($call instanceof CallInterface) &&

                // Check call type unique
                (!in_array(get_class($call), $tabClassPath))
            );
            $tabClassPath[] = get_class($call);
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified list of calls has valid format.
	 * 
     * @param mixed $call
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($call)
    {
		// Init var
		$result =
            // Check valid array
            is_array($call) &&

            // Check valid list of calls
            static::checkTabCallIsValid($call);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($call) ? serialize($call) : $call));
		}
		
		// Return result
		return $result;
    }
	
	
	
}