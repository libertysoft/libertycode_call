<?php
/**
 * Description :
 * This class allows to define default selector class.
 * Can be consider is base of all selector types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\select\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\call\select\api\SelectorInterface;

use liberty_code\call\call\api\CallInterface;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\call\select\library\ConstSelector;
use liberty_code\call\select\exception\FactoryInvalidFormatException;
use liberty_code\call\select\exception\CallInvalidFormatException;
use liberty_code\call\select\exception\CallEnableGetException;



class DefaultSelector extends FixBean implements SelectorInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param CallFactoryInterface $objFactory
     * @param CallInterface[] $tabCall = array()
     */
    public function __construct(CallFactoryInterface $objFactory, array $tabCall = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init call factory
        $this->setFactory($objFactory);

		// Init list of calls, if required
        if(!is_null($tabCall))
        {
            $this->setTabCall($tabCall);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstSelector::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstSelector::DATA_KEY_DEFAULT_FACTORY] = null;
        }
		
		if(!$this->beanExists(ConstSelector::DATA_KEY_DEFAULT_CALL))
        {
            $this->__beanTabData[ConstSelector::DATA_KEY_DEFAULT_CALL] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstSelector::DATA_KEY_DEFAULT_FACTORY,
            ConstSelector::DATA_KEY_DEFAULT_CALL
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSelector::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;
				
				case ConstSelector::DATA_KEY_DEFAULT_CALL:
                    CallInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjFactory()
    {
        // Return result
        return $this->beanGet(ConstSelector::DATA_KEY_DEFAULT_FACTORY);
    }



    /**
     * @inheritdoc
     */
    public function getTabCall()
    {
        // Return result
        return $this->beanGet(ConstSelector::DATA_KEY_DEFAULT_CALL);
    }



    /**
     * Get object call,
     * from specified class path,
     * from specific list of calls.
     *
     * @param string $strCallClassPath
     * @return null|CallInterface
     */
    protected function getObjCallFromClassPath($strCallClassPath)
    {
        // Init var
        $result = null;
        $tabCall = $this->getTabCall();

        // Check minimal requirement
        if(
            is_string($strCallClassPath) &&
            (trim($strCallClassPath) != '') &&
            (count($tabCall) > 0)
        )
        {
            // Get call instance, if found
            for($intCpt = 0; ($intCpt < count($tabCall)) && is_null($result); $intCpt++)
            {
                $call = $tabCall[$intCpt];
                if(get_class($call) == $strCallClassPath)
                {
                    $result = $call;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws CallEnableGetException
     */
    public function getObjCall(array $tabCallConfig)
    {
        // Init var
        $result = null;
        $objFactory = $this->getObjFactory();

        // Get call from class path, if found
        $strCallClassPath = $objFactory->getStrCallClassPath($tabCallConfig);
        if(!is_null($strCallClassPath))
        {
            $result = $this->getObjCallFromClassPath($strCallClassPath);
        }

        // Init call, if required
        if(!is_null($result))
        {
            $result = $objFactory->getObjCall($tabCallConfig, $result);
        }

        // Throw exception, if call impossible to get and hydrate
        if(is_null($result))
        {
            throw new CallEnableGetException(serialize($tabCallConfig));
        }

        // Return result
        return $result;
    }
	
	
	
	
	
    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setFactory(CallFactoryInterface $objFactory)
    {
        $this->beanSet(ConstSelector::DATA_KEY_DEFAULT_FACTORY, $objFactory);
    }



    /**
     * @inheritdoc
     */
    public function setTabCall(array $tabCall)
    {
        $this->beanSet(ConstSelector::DATA_KEY_DEFAULT_CALL, $tabCall);
    }
	
	
	
}