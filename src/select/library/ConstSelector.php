<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\select\library;



class ConstSelector
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_FACTORY = 'objFactory';
    const DATA_KEY_DEFAULT_CALL = 'tabCall';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_FACTORY_INVALID_FORMAT = 'Following call factory "%1$s" invalid! It must be null or a factory object.';
    const EXCEPT_MSG_CALL_INVALID_FORMAT =
        'Following list of calls "%1$s" invalid! 
        The list of calls must be an array of call type instances, where each type must be unique.';
    const EXCEPT_MSG_CALL_ENABLE_GET = 'Impossible to get call from following destination configuration "%1$s"!';
}