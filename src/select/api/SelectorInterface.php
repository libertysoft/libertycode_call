<?php
/**
 * Description :
 * This class allows to describe behavior of selector class.
 * Selector allows to select and provide a specific call object,
 * from a set of predefined call objects,
 * with a specified call factory,
 * and a specified destination configuration.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\select\api;

use liberty_code\call\call\api\CallInterface;
use liberty_code\call\call\factory\api\CallFactoryInterface;



interface SelectorInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get call factory object.
     *
     * @return CallFactoryInterface
     */
    public function getObjFactory();



    /**
     * Get call instances array.
     *
     * @return CallInterface[]
     */
    public function getTabCall();



    /**
     * Get object call,
     * from specified destination configuration array (@see CallFactoryInterface ).
     *
     * @param array $tabCallConfig
     * @return CallInterface
     */
    public function getObjCall(array $tabCallConfig);
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set call factory object.
     *
     * @param CallFactoryInterface $objFactory
     */
    public function setFactory(CallFactoryInterface $objFactory);



    /**
     * Set call instances array.
     *
     * @param CallInterface[] $tabCall
     */
    public function setTabCall(array $tabCall);
}