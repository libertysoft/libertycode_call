<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/call/factory/test/FactoryTest.php');

// Use
use liberty_code\call\call\file\library\ConstFileCall;
use liberty_code\call\call\file\model\FileCall;
use liberty_code\call\call\di\clss\library\ConstClassCall;
use liberty_code\call\call\di\clss\model\ClassCall;
use liberty_code\call\call\di\dependency\library\ConstDependencyCall;
use liberty_code\call\call\di\dependency\model\DependencyCall;
use liberty_code\call\call\di\func\library\ConstFunctionCall;
use liberty_code\call\call\di\func\model\FunctionCall;
use liberty_code\call\call\factory\library\ConstCallFactory;
use liberty_code\call\call\factory\file\library\ConstFileCallFactory;
use liberty_code\call\call\factory\di\library\ConstDiCallFactory;
use liberty_code\call\select\model\DefaultSelector;



// Init var
$objFileCall = $objProvider->getFromClassPath(FileCall::class);
$objClassCall = $objProvider->getFromClassPath(ClassCall::class);
$objDependencyCall = $objProvider->getFromClassPath(DependencyCall::class);
$objFunctionCall = $objProvider->getFromClassPath(FunctionCall::class);

$tabCall = array(
    $objFileCall,
    $objClassCall,
    $objDependencyCall,
    $objFunctionCall
);

$objSelector = new DefaultSelector(
    $objCallFacto,
    $tabCall
);



// Test get call
$tabCallData = array(
    [
        ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstFileCallFactory::CALL_CONFIG_TYPE_FILE,
        ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN => 'call/file/test/FileTest.php'
    ], // Ok

    [
        ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstFileCallFactory::CALL_CONFIG_TYPE_FILE,
        ConstClassCall::TAB_CALL_CONFIG_KEY_CLASS_PATH_PATTERN => 'call\di\clss\test\ControllerTest',
        ConstClassCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => 'action'
    ], // Ko: class call config provide for file call

    [
        ConstClassCall::TAB_CALL_CONFIG_KEY_CLASS_PATH_PATTERN => 'call\di\clss\test\ControllerTest',
        ConstClassCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => 'action'
    ], // Ok

    [
        ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstDiCallFactory::CALL_CONFIG_TYPE_DEPENDENCY,
        ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => '',
        ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => 'action'
    ], // Ko: Dependency call invalid

    [
        ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstDiCallFactory::CALL_CONFIG_TYPE_DEPENDENCY,
        ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => 'svc_1',
        ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => 'action'
    ], // Ok

    [
        ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstDiCallFactory::CALL_CONFIG_TYPE_FUNCTION,
        'test' => 'action'
    ], // Ko: Function call invalid

    [
        ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstDiCallFactory::CALL_CONFIG_TYPE_FUNCTION,
        ConstFunctionCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN => 'call/di/func/test_1/FileTest.php',
        ConstFunctionCall::TAB_CALL_CONFIG_KEY_FUNCTION_NAME_PATTERN => 'action'
    ] // Ok
);

foreach($tabCallData as $callData)
{
    echo('Test get call: <br />');
    echo('<pre>');var_dump($callData);echo('</pre>');

    try{
        $tabCallConfig = $callData;
        $objCall = $objSelector->getObjCall($tabCallConfig);

        if(!is_null($objCall))
        {
            echo('Call class path: <pre>');var_dump(get_class($objCall));echo('</pre>');
            echo('Call config: <pre>');var_dump($objCall->getTabConfig());echo('</pre>');
            echo('Call destination config: <pre>');var_dump($objCall->getTabCallConfig());echo('</pre>');
            echo('Call callable before: <pre>');var_dump($objCall->getCallableBefore());echo('</pre>');
            echo('Call callable after: <pre>');var_dump($objCall->getCallableAfter());echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


