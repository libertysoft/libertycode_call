<?php
/**
 * Description :
 * This class allows to describe behavior of call class.
 * Call allows to get a callback function,
 * to execute specific configured destination (for ex: method, function, file, ...).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\api;



interface CallInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get main configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get destination configuration array.
     *
     * @return array
     */
    public function getTabCallConfig();



    /**
     * Get callback function, used just before execution of specific configured destination.
     * This callback is included on destination callback return.
     *
     * @return null|callable
     */
    public function getCallableBefore();



    /**
     * Get callback function, used just after execution of specific configured destination.
     * This callback is included on destination callback return.
     *
     * @return null|callable
     */
    public function getCallableAfter();



    /**
     * Get callback function,
     * to execute specific configured destination,
     * from specified array of arguments,
     * and specified array of string elements for destination customization.
     *
	 * @param array $tabArg = array()
     * @param array $tabStrElm = array()
     * @param boolean $boolCallBefore = true
     * @param boolean $boolCallAfter = true
     * @return null|callable
     */
    public function getCallable(
        array $tabArg = array(),
        array $tabStrElm = array(),
        $boolCallBefore = true,
        $boolCallAfter = true
    );

	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set main configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);



    /**
     * Set destination configuration array.
     *
     * @param array $tabCallConfig
     */
    public function setCallConfig(array $tabCallConfig);



    /**
     * Set callback function, used just before execution of specific configured destination.
     * This callback is included on destination callback return.
     * Format: void function(array $tabParam):
     * Parameters contain info about context of destination call, and depend of call type.
     *
     * @param null|callable $callable
     */
    public function setCallableBefore($callable);



    /**
     * Set callback function, used just after execution of specific configured destination.
     * This callback is included on destination callback return.
     * Format: void function(array $tabParam):
     * Parameters contain info about context of destination call and result, and depend of call type.
     *
     * @param null|callable $callable
     */
    public function setCallableAfter($callable);
}