<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\di\dependency\exception;

use liberty_code\call\call\di\dependency\library\ConstDependencyCall;



class CallConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDependencyCall::EXCEPT_MSG_CALL_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid dependency key pattern
            isset($config[ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN]) &&
            is_string($config[ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN]) &&
            (trim($config[ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN]) != '') &&

            // Check valid method name pattern
            (
                (!isset($config[ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN])) ||
                (
                    is_string($config[ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN]) &&
                    (trim($config[ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN]) != '')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}