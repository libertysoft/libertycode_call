<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\di\dependency\library;



class ConstDependencyCall
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Destination configuration
    const TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN = 'dependency_key_pattern';
    const TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN = 'method_pattern';

    const CONF_CALL_KEY_SEPARATOR = ':';

    // Callable parameters configuration
    const TAB_CALLABLE_PARAM_ARG_KEY_DEPENDENCY_KEY = 'dependency_key';
    const TAB_CALLABLE_PARAM_ARG_KEY_CLASS_PATH = 'class_path';
    const TAB_CALLABLE_PARAM_ARG_KEY_METHOD_NAME = 'method';
    const TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT = 'argument';
    const TAB_CALLABLE_PARAM_ARG_KEY_INSTANCE = 'instance';



    // Exception message constants
    const EXCEPT_MSG_CALL_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the dependency call destination configuration standard.';
}