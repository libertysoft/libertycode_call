<?php
/**
 * Description :
 * This class allows to define dependency call class.
 * Dependency call is a DI call, allows to configure dependency and method destination.
 *
 * Dependency call uses the following specified configuration:
 * [
 *     DI call configuration
 * ]
 *
 * Dependency call uses the following specified destination configuration:
 * [
 *     dependency_key_pattern(required):
 *         "dependency key pattern / dependency key pattern:method name pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required",
 *
 *     method_pattern(optional: got from dependency_key_pattern if not found):
 *         "method name pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\di\dependency\model;

use liberty_code\call\call\di\model\DiCall;

use liberty_code\call\call\library\ConstCall;
use liberty_code\call\call\library\ToolBoxCall;
use liberty_code\call\call\di\dependency\library\ConstDependencyCall;
use liberty_code\call\call\di\dependency\exception\CallConfigInvalidFormatException;



class DependencyCall extends DiCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG:
                    CallConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws CallConfigInvalidFormatException
     */
    public function getCallable(
        array $tabArg = array(),
        array $tabStrElm = array(),
        $boolCallBefore = true,
        $boolCallAfter = true
    )
    {
        // Init var
        $result = null;
        $objProvider = $this->getObjProvider();
        $tabCallConfig = $this->getTabCallConfig();

        // Check minimal requirement
        if((!is_null($objProvider)) && (count($tabCallConfig) > 0))
        {
            // Get infos from config
            $strDependencyKey = null;
            $strMethodName = null;

            // Method attribute found
            if(array_key_exists(ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN, $tabCallConfig))
            {
                // Get dependency key and method name
                $strDependencyKey = $tabCallConfig[ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN];
                $strMethodName = $tabCallConfig[ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN];
            }
            // Else consider method on dependency key
            else
            {
                // Get class path and method name
                ToolBoxCall::setConfigPathSeparation(
                    $tabCallConfig[ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN],
                    ConstDependencyCall::CONF_CALL_KEY_SEPARATOR,
                    $strDependencyKey,
                    $strMethodName
                );
            }

            // Check config info valid
            if(
                (!is_null($strDependencyKey)) && (!is_null($strMethodName)) &&
                is_string($strDependencyKey) && is_string($strMethodName) &&
                (trim($strDependencyKey) != '') && (trim($strMethodName) != '')
            )
            {
                // Get calculated dependency key and method name
                $strDependencyKey = @$this->getStrValueFormatFromElm($strDependencyKey, $tabStrElm);
                $strMethodName = @$this->getStrValueFormatFromElm($strMethodName, $tabStrElm);

                // Get instance with DI provider
                $objInstance = $objProvider->getFromKey($strDependencyKey);

                // Check instance found
                if (!is_null($objInstance))
                {
                    // Get callable with DI provider
                    $callable = $objProvider->getCallable(array($objInstance, $strMethodName), $tabArg);

                    // Check callable found
                    if(!is_null($callable))
                    {
                        // Wrap callback function
                        $result = $this->getCallableWrap
                        (
                            $callable,
                            array(
                                ConstDependencyCall::TAB_CALLABLE_PARAM_ARG_KEY_DEPENDENCY_KEY => $strDependencyKey,
                                ConstDependencyCall::TAB_CALLABLE_PARAM_ARG_KEY_CLASS_PATH => get_class($objInstance),
                                ConstDependencyCall::TAB_CALLABLE_PARAM_ARG_KEY_METHOD_NAME => $strMethodName,
                                ConstDependencyCall::TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT => $tabArg,
                                ConstDependencyCall::TAB_CALLABLE_PARAM_ARG_KEY_INSTANCE => $objInstance
                            ),
                            $boolCallBefore,
                            $boolCallAfter
                        );
                    }
                }
            }

            // Throw exception, if callable not gettable
            if(is_null($result))
            {
                throw new CallConfigInvalidFormatException(serialize($this->getTabCallConfig()));
            }
        }

        // Return result
        return $result;
    }



}