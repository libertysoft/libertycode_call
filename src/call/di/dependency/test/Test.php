<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/call/di/clss/test/DiTest.php');

// Use
use liberty_code\call\call\library\ConstCall;
use liberty_code\call\call\di\dependency\library\ConstDependencyCall;
use liberty_code\call\call\di\dependency\model\DependencyCall;



// Init var
$tabConfig = array(
    ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_KEY_VALUE,
    ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN => '<%1$s>',
    //ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN => '%1$s'
);

$callableBefore = function (array $tabParam) {
    echo('Test callable before:<pre>');var_dump($tabParam);echo('</pre>');
};

$callableAfter = function (array $tabParam) {
    echo('Test callable after: <pre>');var_dump($tabParam);echo('</pre>');
};

$objCall = new DependencyCall(
    $objProvider,
    $tabConfig,
    null,
    $callableBefore,
    $callableAfter
);



// Test configuration
echo('Test configuration: <br />');

echo('<pre>');print_r($objCall->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test callable wrap
echo('Test callable wrap: <br />');

echo('Get callable before: <pre>');var_dump($objCall->getCallableBefore());echo('</pre>');
echo('Get callable after: <pre>');var_dump($objCall->getCallableAfter());echo('</pre>');

echo('<br /><br /><br />');



// Test call
$tabCallData = array(
    [
        [
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => 'svc_1',
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => 'action'
        ],
        [
            'Value 1'
        ],
        []
    ], // Ok

    [
        [
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => 'svc_2',
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => 'action'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        []
    ], // Ko: dependency not found

    [
        [
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => 'svc_1',
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => 'action_1'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        []
    ], // Ko: method not found

    [
        [
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => '<elm_key_1>',
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => '<elm_key_2>'
        ],
        [
            'Value 1'
        ],
        [
            'elm_key_1' => 'svc_1',
            'elm_key_2' => 'action'
        ]
    ], // Ok

    [
        [
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => '<elm_key_1>',
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => '<elm_key_2>',
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'elm_key_1' => 'svc_2',
            'elm_key_2' => 'action'
        ]
    ], // Ko: dependency not found (on element)

    [
        [
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => '%1$s',
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => '%2$s'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'elm_key_1' => 'svc_1',
            'elm_key_2' => 'action'
        ],
        [
            ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_INDEX
        ]
    ], // Ok

    [
        [
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_DEPENDENCY_KEY_PATTERN => '%1$s',
            ConstDependencyCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN => '%2$s'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'elm_key_1' => 'svc_1',
            'elm_key_2' => 'action'
        ],
        [
            ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_INDEX,
            ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN => '<%1$s>',
        ]
    ] // Ko: Bad configuration format
);

foreach($tabCallData as $callData)
{
    echo('Test call: <br />');
    echo('<pre>');var_dump($callData);echo('</pre>');

    try{
        $tabCallConfig = $callData[0];
        $tabArg = $callData[1];
        $tabStrElm = $callData[2];

        $tabConfigPrevious = null;
        if(isset($callData[3]))
        {
            $tabConfigPrevious = $objCall->getTabConfig();
            $tabConfig = $callData[3];

            $objCall->setConfig($tabConfig);
            echo('Config: <pre>');var_dump($objCall->getTabConfig());echo('</pre>');
        }

        $objCall->setCallConfig($tabCallConfig);
        echo('Call config: <pre>');var_dump($objCall->getTabCallConfig());echo('</pre>');

        $callable = $objCall->getCallable($tabArg, $tabStrElm);
        echo('Call:<br />');print_r($callable());
        echo('<br /><br />');

        $callable = $objCall->getCallable($tabArg, $tabStrElm, false, false);
        echo('Call (without before, after):<br />');print_r($callable());

        if(!is_null($tabConfigPrevious))
        {
            $objCall->setConfig($tabConfigPrevious);
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


