<?php
/**
 * Description :
 * This class allows to define class call class.
 * Class call is a DI call, allows to configure class and method destination.
 *
 * Class call uses the following specified configuration:
 * [
 *     DI call configuration,
 *
 *     class_path_format_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by class path destination",
 *
 *     class_path_format_require(optional: got false if not found): true / false
 * ]
 *
 * Class call uses the following specified destination configuration:
 * [
 *     class_path_pattern(required):
 *         "class path pattern / class path pattern:method name pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required",
 *
 *     method_name_pattern(optional: got from class_path_pattern if not found):
 *         "method name pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required",
 *
 *     class_path_format_require(optional: got class_path_format_require value on configuration, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\di\clss\model;

use liberty_code\call\call\di\model\DiCall;

use liberty_code\call\call\library\ConstCall;
use liberty_code\call\call\library\ToolBoxCall;
use liberty_code\call\call\di\clss\library\ConstClassCall;
use liberty_code\call\call\di\clss\exception\ConfigInvalidFormatException;
use liberty_code\call\call\di\clss\exception\CallConfigInvalidFormatException;



class ClassCall extends DiCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG:
                    CallConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check class path formatting required.
     *
     * @return boolean
     */
    protected function checkClassPathFormatRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $tabCallConfig = $this->getTabCallConfig();
        $result = (
            array_key_exists(ConstClassCall::TAB_CONFIG_KEY_CLASS_PATH_FORMAT_REQUIRE, $tabConfig) &&
            (intval($tabConfig[ConstClassCall::TAB_CONFIG_KEY_CLASS_PATH_FORMAT_REQUIRE]) != 0)
        );

        // Check formatting required
        if(array_key_exists(ConstClassCall::TAB_CALL_CONFIG_KEY_CLASS_PATH_FORMAT_REQUIRE, $tabCallConfig))
        {
            $result = (intval($tabCallConfig[ConstClassCall::TAB_CALL_CONFIG_KEY_CLASS_PATH_FORMAT_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted class path,
     * from specified class path.
     *
     * @param string $strClassPath
     * @param mixed $default = null
     * @return mixed|string
     */
    protected function getStrClassPathFormat($strClassPath, $default = null)
    {
        // Init var
        $result = $default;
        $tabConfig = $this->getTabConfig();

        // Get format pattern
        $strPattern = null;
        if(array_key_exists(ConstClassCall::TAB_CONFIG_KEY_CLASS_PATH_FORMAT_PATTERN, $tabConfig))
        {
            $strPattern = $tabConfig[ConstClassCall::TAB_CONFIG_KEY_CLASS_PATH_FORMAT_PATTERN];
        }

        // Set format, if required
        if($this->checkClassPathFormatRequired() && (!is_null($strPattern)))
        {
            $result = sprintf($strPattern, $strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws CallConfigInvalidFormatException
     */
    public function getCallable(
        array $tabArg = array(),
        array $tabStrElm = array(),
        $boolCallBefore = true,
        $boolCallAfter = true
    )
    {
        // Init var
        $result = null;
        $objProvider = $this->getObjProvider();
        $tabCallConfig = $this->getTabCallConfig();

        // Check minimal requirement
        if((!is_null($objProvider)) && (count($tabCallConfig) > 0))
        {
            // Get infos from config
            $strClassPath = null;
            $strMethodName = null;

            // Method attribute found
            if(array_key_exists(ConstClassCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN, $tabCallConfig))
            {
                // Get class path and method name
                $strClassPath = $tabCallConfig[ConstClassCall::TAB_CALL_CONFIG_KEY_CLASS_PATH_PATTERN];
                $strMethodName = $tabCallConfig[ConstClassCall::TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN];
            }
            // Else consider method on class path
            else
            {
                // Get class path and method name
                ToolBoxCall::setConfigPathSeparation(
                    $tabCallConfig[ConstClassCall::TAB_CALL_CONFIG_KEY_CLASS_PATH_PATTERN],
                    ConstClassCall::CONF_CALL_PATH_SEPARATOR,
                    $strClassPath,
                    $strMethodName
                );
            }

            // Check config info valid
            if(
                (!is_null($strClassPath)) && (!is_null($strMethodName)) &&
                is_string($strClassPath) && is_string($strMethodName) &&
                (trim($strClassPath) != '') && (trim($strMethodName) != '')
            )
            {
                // Get calculated class path and method name
                $strClassPath = @$this->getStrValueFormatFromElm($strClassPath, $tabStrElm);
                $strClassPath = $this->getStrClassPathFormat($strClassPath, $strClassPath);
                $strMethodName = @$this->getStrValueFormatFromElm($strMethodName, $tabStrElm);

                // Get instance with DI provider
                $objInstance = $objProvider->getFromClassPath($strClassPath);

                // Check instance found
                if (!is_null($objInstance))
                {
                    // Get callable with DI provider
                    $callable = $objProvider->getCallable(array($objInstance, $strMethodName), $tabArg);

                    // Check callable found
                    if(!is_null($callable))
                    {
                        // Wrap callback function
                        $result = $this->getCallableWrap
                        (
                            $callable,
                            array(
                                ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_CLASS_PATH => $strClassPath,
                                ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_METHOD_NAME => $strMethodName,
                                ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT => $tabArg,
                                ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_INSTANCE => $objInstance
                            ),
                            $boolCallBefore,
                            $boolCallAfter
                        );
                    }
                }
            }

            // Throw exception, if callable not gettable
            if(is_null($result))
            {
                throw new CallConfigInvalidFormatException(serialize($this->getTabCallConfig()));
            }
        }

        // Return result
        return $result;
    }



}