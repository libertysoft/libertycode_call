<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/call/di/clss/test/ClassTest.php');
require_once($strRootAppPath . '/src/call/di/clss/test/ControllerTest.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\dependency\factory\standard\model\StandardDependencyFactory;
use liberty_code\di\build\model\DefaultBuilder;
use liberty_code\di\provider\model\DefaultProvider;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objDepFactory = new StandardDependencyFactory();
$objDepBuilder = new DefaultBuilder($objDepFactory);

$tabDataSrc = array(
    'svc_1' => [
        'source' => 'liberty_code\\call\\call\\di\\clss\\test\\ClassTest',
        'argument' => [
            ['type' => 'string', 'value' => 'class test update']
        ],
        'option' => [
            //'shared' => true,
            //'class_wired' => 0
        ]
    ]
);

$objDepBuilder->setTabDataSrc($tabDataSrc);
$objDepBuilder->hydrateDependencyCollection($objDepCollection);
$objProvider = new DefaultProvider($objDepCollection);

$objPref = new Preference(array(
    'source' => 'liberty_code\\di\\provider\\api\\ProviderInterface',
    'set' =>  ['type' => 'instance', 'value' => $objProvider],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);


