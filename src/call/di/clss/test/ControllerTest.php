<?php

namespace liberty_code\call\call\di\clss\test;

use liberty_code\call\call\di\clss\test\ClassTest;



class ControllerTest
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var ClassTest */
    protected $objTest;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param ClassTest $objTest
     */
    public function __construct(ClassTest $objTest)
    {
        // Init var
		$this->objTest = $objTest;
    }

	
	
	
	
    // Methods action
    // ******************************************************************************

    public function action($strAdd = '')
    {
        // Return result
        return sprintf(
            'Action controller test: %1$s: %2$s',
            $this->objTest->getStrArg(),
            ((trim($strAdd) != '') ? $strAdd : '-')
        );
    }



}