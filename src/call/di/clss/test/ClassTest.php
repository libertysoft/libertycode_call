<?php

namespace liberty_code\call\call\di\clss\test;



class ClassTest
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var string */
    protected $strArg;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param string $strArg = 'class test'
     */
    public function __construct($strArg = 'class test')
    {
        // Init var
		$this->strArg = $strArg;
    }





    // Methods getters
    // ******************************************************************************

    public function getStrArg()
    {
        // Return result
        return strtoupper(trim($this->strArg));
    }
	
	
	
	
	
	// Methods action
    // ******************************************************************************

    public function action($strAdd = '', $strAdd2 = '')
    {
        // Return result
        return sprintf(
            'Action %1$s: %2$s: %3$s',
            $this->getStrArg(),
            ((trim($strAdd) != '') ? $strAdd : '-'),
            ((trim($strAdd2) != '') ? $strAdd2 : '-')
        );
    }



}