<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\di\clss\library;



class ConstClassCall
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_CLASS_PATH_FORMAT_PATTERN = 'class_path_format_pattern';
    const TAB_CONFIG_KEY_CLASS_PATH_FORMAT_REQUIRE = 'class_path_format_require';

    // Destination configuration
    const TAB_CALL_CONFIG_KEY_CLASS_PATH_PATTERN = 'class_path_pattern';
    const TAB_CALL_CONFIG_KEY_METHOD_NAME_PATTERN = 'method_name_pattern';
    const TAB_CALL_CONFIG_KEY_CLASS_PATH_FORMAT_REQUIRE = 'class_path_format_require';

    const CONF_CALL_PATH_SEPARATOR = ':';

    // Callable parameters configuration
    const TAB_CALLABLE_PARAM_ARG_KEY_CLASS_PATH = 'class_path';
    const TAB_CALLABLE_PARAM_ARG_KEY_METHOD_NAME = 'method_name';
    const TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT = 'argument';
    const TAB_CALLABLE_PARAM_ARG_KEY_INSTANCE = 'instance';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the class call configuration standard.';
    const EXCEPT_MSG_CALL_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the class call destination configuration standard.';
}