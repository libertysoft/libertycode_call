<?php
/**
 * Description :
 * This class allows to define DI call class.
 * DI call is a call, allows to execute specific configured destination,
 * from specified DI provider.
 *
 * DI call uses the following specified configuration:
 * [
 *     Default call configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\di\model;

use liberty_code\call\call\model\DefaultCall;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\call\call\di\library\ConstDiCall;
use liberty_code\call\call\di\exception\ProviderInvalidFormatException;



/**
 * @method ProviderInterface getObjProvider() Get DI provider.
 * @method void setObjProvider(ProviderInterface $objProvider) Set DI provider.
 */
abstract class DiCall extends DefaultCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objProvider
     */
    public function __construct(
        ProviderInterface $objProvider,
        array $tabConfig = null,
        array $tabCallConfig = null,
        callable $callableBefore = null,
        callable $callableAfter = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $tabCallConfig,
            $callableBefore,
            $callableAfter
        );

        // Init DI provider
        $this->setObjProvider($objProvider);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDiCall::DATA_KEY_DEFAULT_PROVIDER))
        {
            $this->__beanTabData[ConstDiCall::DATA_KEY_DEFAULT_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDiCall::DATA_KEY_DEFAULT_PROVIDER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDiCall::DATA_KEY_DEFAULT_PROVIDER:
                    ProviderInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



}