<?php
/**
 * Description :
 * This class allows to define function call class.
 * Function call is a DI call, allows to configure function destination.
 *
 * Function call uses the following specified configuration:
 * [
 *     DI call configuration,
 *
 *     file_path_format_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by file path destination",
 *
 *     file_path_format_require(optional: got false if not found): true / false
 * ]
 *
 * Function call uses the following specified destination configuration:
 * [
 *     function_name_pattern(required/optional: got in file_path_pattern if not found):
 *         "function name pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required",
 *
 *     file_path_pattern(required/optional):
 *         "file path pattern to include previously / file path pattern:function name pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required",
 *
 *     file_path_format_require(optional: got file_path_format_require value on configuration, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\di\func\model;

use liberty_code\call\call\di\model\DiCall;

use liberty_code\call\call\library\ConstCall;
use liberty_code\call\call\library\ToolBoxCall;
use liberty_code\call\call\di\func\library\ConstFunctionCall;
use liberty_code\call\call\di\func\exception\ConfigInvalidFormatException;
use liberty_code\call\call\di\func\exception\CallConfigInvalidFormatException;



class FunctionCall extends DiCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG:
                    CallConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check file path formatting required.
     *
     * @return boolean
     */
    protected function checkFilePathFormatRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $tabCallConfig = $this->getTabCallConfig();
        $result = (
            array_key_exists(ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE, $tabConfig) &&
            (intval($tabConfig[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) != 0)
        );

        // Check formatting required
        if(array_key_exists(ConstFunctionCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE, $tabCallConfig))
        {
            $result = (intval($tabCallConfig[ConstFunctionCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted file path,
     * from specified file path.
     *
     * @param string $strFilePath
     * @param mixed $default = null
     * @return mixed|string
     */
    protected function getStrFilePathFormat($strFilePath, $default = null)
    {
        // Init var
        $result = $default;
        $tabConfig = $this->getTabConfig();

        // Get format pattern
        $strPattern = null;
        if(array_key_exists(ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN, $tabConfig))
        {
            $strPattern = $tabConfig[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN];
        }

        // Set format, if required
        if($this->checkFilePathFormatRequired() && (!is_null($strPattern)))
        {
            $result = sprintf($strPattern, $strFilePath);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws CallConfigInvalidFormatException
     */
    public function getCallable(
        array $tabArg = array(),
        array $tabStrElm = array(),
        $boolCallBefore = true,
        $boolCallAfter = true
    )
    {
        // Init var
        $result = null;
        $objProvider = $this->getObjProvider();
        $tabCallConfig = $this->getTabCallConfig();

        // Check minimal requirement
        if((!is_null($objProvider)) && (count($tabCallConfig) > 0))
        {
            // Get infos from config
            $strFilePath = null;
            $strFunctionName = null;

            // Function attribute found
            if(array_key_exists(ConstFunctionCall::TAB_CALL_CONFIG_KEY_FUNCTION_NAME_PATTERN, $tabCallConfig))
            {
                // Get function name
                $strFunctionName = $tabCallConfig[ConstFunctionCall::TAB_CALL_CONFIG_KEY_FUNCTION_NAME_PATTERN];

                // Get file path, if required
                if(array_key_exists(ConstFunctionCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN, $tabCallConfig))
                {
                    $strFilePath = $tabCallConfig[ConstFunctionCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN];
                }
            }
            // Else consider function on file path
            else
            {
                // Get file path and function name
                ToolBoxCall::setConfigPathSeparation(
                    $tabCallConfig[ConstFunctionCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN],
                    ConstFunctionCall::CONF_CALL_PATH_SEPARATOR,
                    $strFilePath,
                    $strFunctionName
                );
            }

            // Check config info valid
            if(
                (!is_null($strFunctionName)) &&
                is_string($strFunctionName) &&
                (trim($strFunctionName) != '')
            )
            {
                $tabCallableParam = array();

                // Get calculated function name
                $strFunctionName = @$this->getStrValueFormatFromElm($strFunctionName, $tabStrElm);

                // Include file, if required
                if(
                    (!is_null($strFilePath)) &&
                    is_string($strFilePath) &&
                    (trim($strFilePath) != '')
                )
                {
                    // Get calculated file path
                    $strFilePath = @$this->getStrValueFormatFromElm($strFilePath, $tabStrElm);
                    $strFilePath = $this->getStrFilePathFormat($strFilePath, $strFilePath);

                    if(file_exists($strFilePath) && is_file($strFilePath))
                    {
                        require_once($strFilePath);
                    }
                    // Throw exception, if file not valid
                    else
                    {
                        throw new CallConfigInvalidFormatException(serialize($this->getTabCallConfig()));
                    }

                    $tabCallableParam[ConstFunctionCall::TAB_CALLABLE_PARAM_ARG_KEY_FILE_PATH] = $strFilePath;
                }

                // Check function found
                if(function_exists($strFunctionName))
                {
                    // Get callable with DI provider
                    $callable = $objProvider->getCallable($strFunctionName, $tabArg);

                    // Check callable found
                    if(!is_null($callable))
                    {
                        // Wrap callback function
                        $tabCallableParam[ConstFunctionCall::TAB_CALLABLE_PARAM_ARG_KEY_FUNCTION_NAME] = $strFunctionName;
                        $tabCallableParam[ConstFunctionCall::TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT] = $tabArg;

                        $result = $this->getCallableWrap(
                            $callable,
                            $tabCallableParam,
                            $boolCallBefore,
                            $boolCallAfter
                        );
                    }
                }
            }

            // Throw exception, if callable not gettable
            if(is_null($result))
            {
                throw new CallConfigInvalidFormatException(serialize($this->getTabCallConfig()));
            }
        }

        // Return result
        return $result;
    }



}