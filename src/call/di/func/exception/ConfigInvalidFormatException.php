<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\di\func\exception;

use liberty_code\call\call\di\func\library\ConstFunctionCall;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFunctionCall::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid file path format pattern
            (
                (!isset($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN])) ||
                (
                    is_string($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN]) &&
                    (trim($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN]) != '')
                )
            ) &&

            // Check valid file path format required option
            (
                (!isset($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) ||
                    is_int($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) ||
                    (
                        is_string($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) &&
                        ctype_digit($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE])
                    )
                )
            ) &&

            // Check minimum requirement
            (
                isset($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN]) ||
                (!isset($config[ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]))
            );

        // Return result
        return $result;
    }



    /**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}