<?php

use liberty_code\call\call\di\clss\test\ControllerTest;



function getStrPatternTest()
{
    // Return result
    return 'Action file test: %1$s';
}



function action(ControllerTest $objTest, $strAdd)
{
    // Return result
    return sprintf(
        getStrPatternTest(),
        $objTest->action($strAdd)
    );
}


