<?php
/**
 * Description :
 * This class allows to define default call class.
 * Can be consider is base of all call types.
 *
 * Default call uses the following specified configuration:
 * [
 *     elm_replace_type(optional: got 'index' if elm_replace_type not found):
 *         // Array of string elements considered as index array,
 *         // where elements replaced on specific value, considered as sprintf pattern
 *         "index"
 *
 *         OR
 *
 *         // Array of string elements considered as associative array,
 *         // where element keys replaced on specific value, by element values
 *         "key_value",
 *
 *     // Used only if elm_replace_type:'key_value', format element keys, during replacement
 *     elm_format_key_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by element key",
 *
 *     // Used only if elm_replace_type:'key_value', format element values, during replacement
 *     elm_format_value_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by element value",
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\call\call\api\CallInterface;

use Throwable;
use liberty_code\call\call\library\ConstCall;
use liberty_code\call\call\exception\ConfigInvalidFormatException;
use liberty_code\call\call\exception\CallConfigInvalidFormatException;
use liberty_code\call\call\exception\CallableInvalidFormatException;
use liberty_code\call\call\exception\ElementInvalidFormatException;



abstract class DefaultCall extends FixBean implements CallInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param array $tabCallConfig = null
     * @param callable $callableBefore = null
     * @param callable $callableAfter = null
     */
    public function __construct(
        array $tabConfig = null,
        array $tabCallConfig = null,
        $callableBefore = null,
        $callableAfter = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }

        // Init destination configuration, if required
        if(!is_null($tabCallConfig))
        {
            $this->setCallConfig($tabCallConfig);
        }

        // Init callables
        $this->setCallableBefore($callableBefore);
        $this->setCallableAfter($callableAfter);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCall::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstCall::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG))
        {
            $this->__beanTabData[ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG] = array();
        }

        if(!$this->beanExists(ConstCall::DATA_KEY_DEFAULT_CALLABLE_BEFORE))
        {
            $this->__beanTabData[ConstCall::DATA_KEY_DEFAULT_CALLABLE_BEFORE] = null;
        }

        if(!$this->beanExists(ConstCall::DATA_KEY_DEFAULT_CALLABLE_AFTER))
        {
            $this->__beanTabData[ConstCall::DATA_KEY_DEFAULT_CALLABLE_AFTER] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstCall::DATA_KEY_DEFAULT_CONFIG,
            ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG,
            ConstCall::DATA_KEY_DEFAULT_CALLABLE_BEFORE,
            ConstCall::DATA_KEY_DEFAULT_CALLABLE_AFTER
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG:
                    CallConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstCall::DATA_KEY_DEFAULT_CALLABLE_BEFORE:
                case ConstCall::DATA_KEY_DEFAULT_CALLABLE_AFTER:
                    CallableInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstCall::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * @inheritdoc
     */
    public function getTabCallConfig()
    {
        // Return result
        return $this->beanGet(ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG);
    }



    /**
     * @inheritdoc
     */
    public function getCallableBefore()
    {
        // Return result
        return $this->beanGet(ConstCall::DATA_KEY_DEFAULT_CALLABLE_BEFORE);
    }



    /**
     * @inheritdoc
     */
    public function getCallableAfter()
    {
        // Return result
        return $this->beanGet(ConstCall::DATA_KEY_DEFAULT_CALLABLE_AFTER);
    }



    /**
     * Get specified value updated
     * with specified array of custom string elements.
     *
     * @param string $strValue
     * @param array $tabStrElm
     * @return string
     * @throws ElementInvalidFormatException
     */
    protected function getStrValueFormatFromElm($strValue, array $tabStrElm)
    {
        // Check arguments
        ElementInvalidFormatException::setCheck($tabStrElm);

        // Init var
        $result = strval($strValue);
        $tabConfig = $this->getTabConfig();

        // Check replacement required
        if(count($tabStrElm) > 0)
        {
            // Case key-value replacement required: replace element keys by element values
            if(
                array_key_exists(ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE, $tabConfig) &&
                ($tabConfig[ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE] == ConstCall::ELM_REPLACE_TYPE_CONFIG_KEY_VALUE)
            )
            {
                // Format array of elements, if required
                $tabStrElmFormat = array();
                foreach($tabStrElm as $key => $value)
                {
                    if(isset($tabConfig[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN]))
                    {
                        $key = sprintf($tabConfig[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN], strval($key));
                    }
                    else if(isset($tabConfig[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN]))
                    {
                        $value = sprintf($tabConfig[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN], strval($value));
                    }

                    $tabStrElmFormat[$key] = $value;
                }

                // Replace by elements
                $result = str_replace(
                    array_keys($tabStrElmFormat),
                    array_values($tabStrElmFormat),
                    $result
                );
            }
            // Case else: index replacement required: apply sprintf pattern with element values
            else
            {
                try{
                    $result = (is_string($value = vsprintf($result, array_values($tabStrElm))) ? $value : $result);
                }
                catch(Throwable $e)
                {
                    // Do nothing
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get callback function wrapping destination callable.
     *
     * @param callable $callable
     * @param array $tabParam = array()
     * @param boolean $boolCallBefore = true
     * @param boolean $boolCallAfter = true
     * @return null|callable
     */
    protected function getCallableWrap(
        $callable,
        array $tabParam = array(),
        $boolCallBefore = true,
        $boolCallAfter = true
    )
    {
        // Init var
        $result = null;
        $boolCallBefore = (is_bool($boolCallBefore) ? $boolCallBefore : true);
        $boolCallAfter = (is_bool($boolCallAfter) ? $boolCallAfter : true);

        // Check call back function is valid
        if(is_callable($callable))
        {
            // Set callback function main
            $result = function () use ($callable, $tabParam, $boolCallBefore, $boolCallAfter)
            {
                // Execute callback function before, if required
                $callableBefore = $this->getCallableBefore();
                if($boolCallBefore && (!is_null($callableBefore)))
                {
                    $callableBefore($tabParam);
                }

                // Execute destination and get result
                $result = $callable();

                // Execute callback function after, if required
                $callableAfter = $this->getCallableAfter();
                if($boolCallAfter && (!is_null($callableAfter)))
                {
                    $tabParam[ConstCall::TAB_CALLABLE_PARAM_ARG_KEY_RESULT] = $result;
                    $callableAfter($tabParam);
                }

                // Return result
                return $result;
            };
        }

        // Return result
        return $result;
    }
	
	
	
	

    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setConfig(array $tabConfig)
	{
		$this->beanSet(ConstCall::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
	}



    /**
     * @inheritdoc
     */
    public function setCallConfig(array $tabCallConfig)
    {
        $this->beanSet(ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG, $tabCallConfig);
    }



    /**
     * @inheritdoc
     */
    public function setCallableBefore($callable)
    {
        $this->beanSet(ConstCall::DATA_KEY_DEFAULT_CALLABLE_BEFORE, $callable);
    }



    /**
     * @inheritdoc
     */
    public function setCallableAfter($callable)
    {
        $this->beanSet(ConstCall::DATA_KEY_DEFAULT_CALLABLE_AFTER, $callable);
    }



}