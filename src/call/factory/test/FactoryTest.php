<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/call/di/clss/test/DiTest.php');

// Use
use liberty_code\call\call\library\ConstCall;
use liberty_code\call\call\file\library\ConstFileCall;
use liberty_code\call\call\di\clss\library\ConstClassCall;
use liberty_code\call\call\di\func\library\ConstFunctionCall;
use liberty_code\call\call\factory\file\model\FileCallFactory;
use liberty_code\call\call\factory\di\model\DiCallFactory;



// Init var
$tabConfig = array(
    ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_KEY_VALUE,
    ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN => '<%1$s>',
    //ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN => '%1$s',
    ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '/%1$s',
    ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true,
    ConstClassCall::TAB_CONFIG_KEY_CLASS_PATH_FORMAT_PATTERN => 'liberty_code\call\%1$s',
    ConstClassCall::TAB_CONFIG_KEY_CLASS_PATH_FORMAT_REQUIRE => true,
    ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '/%1$s',
    ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true
);

$callableBefore = function (array $tabParam) {
    echo('Test callable before:<pre>');var_dump($tabParam);echo('</pre>');
};

$callableAfter = function (array $tabParam) {
    echo('Test callable after: <pre>');var_dump($tabParam);echo('</pre>');
};

$objCallFacto = new FileCallFactory(
    $tabConfig,
    $callableBefore,
    $callableAfter
);

$objCallFacto = new DiCallFactory(
    $objProvider,
    $tabConfig,
    $callableBefore,
    $callableAfter,
    $objCallFacto
);


