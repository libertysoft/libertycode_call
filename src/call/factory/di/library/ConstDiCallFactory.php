<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\factory\di\library;



class ConstDiCallFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CALL_PROVIDER = 'objCallProvider';



    // Type configuration
    const CALL_CONFIG_TYPE_CLASS = 'class';
    const CALL_CONFIG_TYPE_DEPENDENCY = 'dependency';
    const CALL_CONFIG_TYPE_FUNCTION = 'function';
}