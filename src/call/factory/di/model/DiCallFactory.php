<?php
/**
 * Description :
 * This class allows to define DI call factory class.
 * DI call factory allows to provide and hydrate DI call instance.
 *
 * DI call factory uses the following specified configuration, to hydrate call:
 * [
 *     Class call configuration (@see ClassCall )
 *
 *     AND\OR
 *
 *     Dependency call configuration (@see DependencyCall )
 *
 *     AND\OR
 *
 *     Function call configuration (@see FunctionCall )
 * ]
 *
 * DI call factory uses the following specified destination configuration, to get and hydrate call:
 * [
 *     type(optional): "class",
 *     Class call destination configuration (@see ClassCall )
 *
 *     OR
 *
 *     type(required): "dependency",
 *     Dependency call destination configuration (@see DependencyCall )
 *
 *     OR
 *
 *     type(required): "function",
 *     Function call destination configuration (@see FunctionCall )
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\factory\di\model;

use liberty_code\call\call\factory\model\DefaultCallFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\call\call\api\CallInterface;
use liberty_code\call\call\di\model\DiCall;
use liberty_code\call\call\di\exception\ProviderInvalidFormatException;
use liberty_code\call\call\di\clss\model\ClassCall;
use liberty_code\call\call\di\dependency\model\DependencyCall;
use liberty_code\call\call\di\func\model\FunctionCall;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\call\call\factory\di\library\ConstDiCallFactory;



/**
 * @method ProviderInterface getObjCallProvider() Get DI provider object, used on call destination.
 * @method void setObjCallProvider(ProviderInterface $objCallProvider) Set DI provider object, used on call destination.
 */
class DiCallFactory extends DefaultCallFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objCallProvider
     */
    public function __construct(
        ProviderInterface $objCallProvider,
        array $tabConfig = null,
        $callableBefore = null,
        $callableAfter = null,
        CallFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $callableBefore,
            $callableAfter,
            $objFactory,
            $objProvider
        );

        // Init DI provider, used on call destination
        $this->setObjCallProvider($objCallProvider);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDiCallFactory::DATA_KEY_DEFAULT_CALL_PROVIDER))
        {
            $this->__beanTabData[ConstDiCallFactory::DATA_KEY_DEFAULT_CALL_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    protected function hydrateCall(CallInterface $objCall, array $tabCallConfig)
    {
        // Hydrate DI call, if required
        if($objCall instanceof DiCall)
        {
            $objCall->setObjProvider($this->getObjCallProvider());
        }

        // Return result
        return parent::hydrateCall($objCall, $tabCallConfig);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDiCallFactory::DATA_KEY_DEFAULT_CALL_PROVIDER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDiCallFactory::DATA_KEY_DEFAULT_CALL_PROVIDER:
                    ProviderInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrCallClassPathFromType($strCallConfigType)
    {
        // Init var
        $result = null;

        // Get class path of call, from type
        switch($strCallConfigType)
        {
            case null:
            case ConstDiCallFactory::CALL_CONFIG_TYPE_CLASS:
                $result = ClassCall::class;
                break;

            case ConstDiCallFactory::CALL_CONFIG_TYPE_DEPENDENCY:
                $result = DependencyCall::class;
                break;

            case ConstDiCallFactory::CALL_CONFIG_TYPE_FUNCTION:
                $result = FunctionCall::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjCallNew($strCallConfigType)
    {
        // Init var
        $result = parent::getObjCallNew($strCallConfigType);

        if(is_null($result))
        {
            $objCallProvider = $this->getObjCallProvider();

            // Get class path of call, from type
            switch($strCallConfigType)
            {
                case null:
                case ConstDiCallFactory::CALL_CONFIG_TYPE_CLASS:
                    $result = new ClassCall($objCallProvider);
                    break;

                case ConstDiCallFactory::CALL_CONFIG_TYPE_DEPENDENCY:
                    $result = new DependencyCall($objCallProvider);
                    break;

                case ConstDiCallFactory::CALL_CONFIG_TYPE_FUNCTION:
                    $result = new FunctionCall($objCallProvider);
                    break;
            }
        }

        // Return result
        return $result;
    }



}