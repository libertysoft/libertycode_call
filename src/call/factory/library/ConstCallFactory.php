<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\factory\library;



class ConstCallFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_FACTORY = 'objFactory';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CALLABLE_BEFORE = 'callableBefore';
    const DATA_KEY_DEFAULT_CALLABLE_AFTER = 'callableAfter';



    // Destination configuration
    const TAB_CALL_CONFIG_KEY_TYPE = 'type';



    // Exception message constants
    const EXCEPT_MSG_FACTORY_INVALID_FORMAT = 'Following call factory "%1$s" invalid! It must be null or a factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default call factory configuration standard.';
    const EXCEPT_MSG_CALL_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default call factory destination configuration standard.';
}