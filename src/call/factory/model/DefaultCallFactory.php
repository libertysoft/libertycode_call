<?php
/**
 * Description :
 * This class allows to define default call factory class.
 * Can be consider is base of all call factory type.
 *
 * Default call factory uses the following specified destination configuration, to get and hydrate call:
 * [
 *     type(optional): "string constant to determine call type",
 *
 *     ... specific call destination configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\call\call\factory\api\CallFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\call\call\api\CallInterface;
use liberty_code\call\call\exception\CallableInvalidFormatException;
use liberty_code\call\call\factory\library\ConstCallFactory;
use liberty_code\call\call\factory\exception\FactoryInvalidFormatException;
use liberty_code\call\call\factory\exception\ConfigInvalidFormatException;
use liberty_code\call\call\factory\exception\CallConfigInvalidFormatException;



/**
 * @method null|CallFactoryInterface getObjFactory() Get parent factory object.
 * @method array getTabConfig() Get configuration array, used for call main configuration.
 * @method array getCallableBefore() Get callback function, used for call callback before function (@see CallInterface::getCallableBefore() ).
 * @method array getCallableAfter() Get callback function, used for call callback after function (@see CallInterface::getCallableAfter() ).
 * @method void setObjFactory(null|CallFactoryInterface $objFactory) Set parent factory object.
 * @method void setTabConfig(array $tabConfig) Set configuration array, used for call main configuration.
 * @method void setCallableBefore(null|callable $callable) Set callback function, used for call callback before function (@see CallInterface::setCallableBefore() ).
 * @method void setCallableAfter(null|callable $callable) Set callback function, used for call callback after function (@see CallInterface::setCallableAfter() ).
 */
abstract class DefaultCallFactory extends DefaultFactory implements CallFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param callable $callableBefore = null
     * @param callable $callableAfter = null
     * @param CallFactoryInterface $objFactory = null
     */
    public function __construct(
        array $tabConfig = null,
        $callableBefore = null,
        $callableAfter = null,
        CallFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init callables
        $this->setCallableBefore($callableBefore);
        $this->setCallableAfter($callableAfter);

        // Init call factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCallFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstCallFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        if(!$this->beanExists(ConstCallFactory::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstCallFactory::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstCallFactory::DATA_KEY_DEFAULT_CALLABLE_BEFORE))
        {
            $this->__beanTabData[ConstCallFactory::DATA_KEY_DEFAULT_CALLABLE_BEFORE] = null;
        }

        if(!$this->beanExists(ConstCallFactory::DATA_KEY_DEFAULT_CALLABLE_AFTER))
        {
            $this->__beanTabData[ConstCallFactory::DATA_KEY_DEFAULT_CALLABLE_AFTER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified call.
     * Overwrite it to set specific call hydration.
     *
     * @param CallInterface $objCall
     * @param array $tabCallConfig
     * @throws CallConfigInvalidFormatException
     */
    protected function hydrateCall(CallInterface $objCall, array $tabCallConfig)
    {
        // Init destination configuration
        if(array_key_exists(ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE, $tabCallConfig))
        {
            unset($tabCallConfig[ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE]);
        }

        // Hydrate call
        $objCall->setConfig($this->getTabConfig());
        $objCall->setCallConfig($tabCallConfig);
        $objCall->setCallableBefore($this->getCallableBefore());
        $objCall->setCallableAfter($this->getCallableAfter());
    }


    
    

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstCallFactory::DATA_KEY_DEFAULT_FACTORY,
            ConstCallFactory::DATA_KEY_DEFAULT_CONFIG,
            ConstCallFactory::DATA_KEY_DEFAULT_CALLABLE_BEFORE,
            ConstCallFactory::DATA_KEY_DEFAULT_CALLABLE_AFTER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCallFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstCallFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstCallFactory::DATA_KEY_DEFAULT_CALLABLE_BEFORE:
                case ConstCallFactory::DATA_KEY_DEFAULT_CALLABLE_AFTER:
                    CallableInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified destination configuration is valid,
     * for the specified call object.
     *
     * @param CallInterface $objCall
     * @param array $tabCallConfig
     * @return boolean
     * @throws CallConfigInvalidFormatException
     */
    protected function checkCallConfigIsValid(CallInterface $objCall, array $tabCallConfig)
    {
        // Init var
        $strCallClassPath = $this->getStrCallClassPathEngine($tabCallConfig);
        $result = (
            (!is_null($strCallClassPath)) &&
            ($strCallClassPath == get_class($objCall))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string configured type,
     * from specified destination configuration.
     *
     * @param array $tabCallConfig
     * @return null|string
     * @throws CallConfigInvalidFormatException
     */
    protected function getStrCallConfigType(array $tabCallConfig)
    {
        // Check arguments
        CallConfigInvalidFormatException::setCheck($tabCallConfig);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE, $tabCallConfig))
        {
            $result = $tabCallConfig[ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of call,
     * from specified destination configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strCallConfigType
     * @return null|string
     */
    abstract protected function getStrCallClassPathFromType($strCallConfigType);



    /**
     * Get string class path of call engine.
     *
     * @param array $tabCallConfig
     * @return null|string
     * @throws CallConfigInvalidFormatException
     */
    protected function getStrCallClassPathEngine(array $tabCallConfig)
    {
        // Init var
        $strCallConfigType = $this->getStrCallConfigType($tabCallConfig);
        $result = $this->getStrCallClassPathFromType($strCallConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrCallClassPath(array $tabCallConfig)
    {
        // Init var
        $result = $this->getStrCallClassPathEngine($tabCallConfig);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrCallClassPath($tabCallConfig);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance call,
     * from specified destination configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strCallConfigType
     * @return null|CallInterface
     */
    protected function getObjCallNew($strCallConfigType)
    {
        // Init var
        $strClassPath = $this->getStrCallClassPathFromType($strCallConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance call engine.
     *
     * @param array $tabCallConfig
     * @param CallInterface $objCall = null
     * @return null|CallInterface
     */
    protected function getObjCallEngine(array $tabCallConfig, CallInterface $objCall = null)
    {
        // Init var
        $result = null;
        $strCallConfigType = $this->getStrCallConfigType($tabCallConfig);
        $objCall = (
            is_null($objCall) ?
                $this->getObjCallNew($strCallConfigType) :
                $objCall
        );

        // Get and hydrate call, if required
        if(
            (!is_null($objCall)) &&
            $this->checkCallConfigIsValid($objCall, $tabCallConfig)
        )
        {
            $this->hydrateCall($objCall, $tabCallConfig);
            $result = $objCall;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjCall(array $tabCallConfig, CallInterface $objCall = null)
    {
        // Init var
        $result = $this->getObjCallEngine($tabCallConfig, $objCall);

        // Get call from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjCall($tabCallConfig, $objCall);
        }

        // Return result
        return $result;
    }



}