<?php
/**
 * Description :
 * This class allows to define file call factory class.
 * File call factory allows to provide and hydrate file call instance.
 *
 * File call factory uses the following specified configuration, to hydrate call:
 * [
 *     File call configuration (@see FileCall )
 * ]
 *
 * File call factory uses the following specified destination configuration, to get and hydrate call:
 * [
 *     type(optional): "file",
 *     File call destination configuration (@see FileCall )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\factory\file\model;

use liberty_code\call\call\factory\model\DefaultCallFactory;

use liberty_code\call\call\file\model\FileCall;
use liberty_code\call\call\factory\file\library\ConstFileCallFactory;



class FileCallFactory extends DefaultCallFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrCallClassPathFromType($strCallConfigType)
    {
        // Init var
        $result = null;

        // Get class path of call, from type
        switch($strCallConfigType)
        {
            case null:
            case ConstFileCallFactory::CALL_CONFIG_TYPE_FILE:
                $result = FileCall::class;
                break;
        }

        // Return result
        return $result;
    }

	
	
}