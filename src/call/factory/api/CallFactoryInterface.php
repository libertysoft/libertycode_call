<?php
/**
 * Description :
 * This class allows to describe behavior of call factory class.
 * Call factory allows to provide new or specified call instance,
 * hydrated with a specified destination configuration,
 * from a set of potential predefined call types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\factory\api;

use liberty_code\call\call\api\CallInterface;



interface CallFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of call,
     * from specified destination configuration array.
     *
     * @param array $tabCallConfig
     * @return null|string
     */
    public function getStrCallClassPath(array $tabCallConfig);



    /**
     * Get new or specified object instance call,
     * hydrated from specified destination configuration array.
     *
     * @param array $tabCallConfig
     * @param CallInterface $objCall = null
     * @return null|CallInterface
     */
    public function getObjCall(array $tabCallConfig, CallInterface $objCall = null);
}