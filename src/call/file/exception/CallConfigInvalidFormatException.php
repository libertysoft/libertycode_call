<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\file\exception;

use liberty_code\call\call\file\library\ConstFileCall;



class CallConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFileCall::EXCEPT_MSG_CALL_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid file path pattern
            isset($config[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN]) &&
            is_string($config[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN]) &&
            (trim($config[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN]) != '') &&

            // Check valid file path format required option
            (
                (!isset($config[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) ||
                    is_int($config[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) ||
                    (
                        is_string($config[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) &&
                        ctype_digit($config[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}