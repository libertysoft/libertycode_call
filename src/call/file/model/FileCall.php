<?php
/**
 * Description :
 * This class allows to define file call class.
 * File call is a call, allows to configure file destination.
 *
 * File call uses the following specified configuration:
 * [
 *     Default call configuration,
 *
 *     file_path_format_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by file path destination",
 *
 *     file_path_format_require(optional: got false if not found): true / false
 * ]
 *
 * File call uses the following specified destination configuration:
 * [
 *     file_path_pattern(required):
 *         "file path pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required",
 *
 *     file_path_format_require(optional: got file_path_format_require value on configuration, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\file\model;

use liberty_code\call\call\model\DefaultCall;

use liberty_code\call\call\library\ConstCall;
use liberty_code\call\call\file\library\ConstFileCall;
use liberty_code\call\call\file\exception\ConfigInvalidFormatException;
use liberty_code\call\call\file\exception\CallConfigInvalidFormatException;



class FileCall extends DefaultCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG:
                    CallConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check file path formatting required.
     *
     * @return boolean
     */
    protected function checkFilePathFormatRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $tabCallConfig = $this->getTabCallConfig();
        $result = (
            array_key_exists(ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE, $tabConfig) &&
            (intval($tabConfig[ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) != 0)
        );

        // Check formatting required
        if(array_key_exists(ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE, $tabCallConfig))
        {
            $result = (intval($tabCallConfig[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted file path,
     * from specified file path.
     *
     * @param string $strFilePath
     * @param mixed $default = null
     * @return mixed|string
     */
    protected function getStrFilePathFormat($strFilePath, $default = null)
    {
        // Init var
        $result = $default;
        $tabConfig = $this->getTabConfig();

        // Get format pattern
        $strPattern = null;
        if(array_key_exists(ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN, $tabConfig))
        {
            $strPattern = $tabConfig[ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN];
        }

        // Set format, if required
        if($this->checkFilePathFormatRequired() && (!is_null($strPattern)))
        {
            $result = sprintf($strPattern, $strFilePath);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws CallConfigInvalidFormatException
     */
    public function getCallable(
        array $tabArg = array(),
        array $tabStrElm = array(),
        $boolCallBefore = true,
        $boolCallAfter = true
    )
    {
        // Init var
        $result = null;
        $tabCallConfig = $this->getTabCallConfig();

        // Check minimal requirement
        if(count($tabCallConfig) > 0)
        {
            // Get infos from config
            $strFilePath = $tabCallConfig[ConstFileCall::TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN];

            // Check config info valid
            if(
                (!is_null($strFilePath)) &&
                is_string($strFilePath) &&
                (trim($strFilePath) != '')
            )
            {
                // Get calculated file path
                $strFilePath = @$this->getStrValueFormatFromElm($strFilePath, $tabStrElm);
                $strFilePath = $this->getStrFilePathFormat($strFilePath, $strFilePath);

                // Check file path is valid
                if(file_exists($strFilePath) && is_file($strFilePath))
                {
                    // Set callback function
                    $result = function () use ($strFilePath, $tabArg)
                    {
                        // Arguments array passed in 'use' to be available in require

                        // Return result
                        return require($strFilePath);
                    };

                    // Wrap callback function
                    $result = $this->getCallableWrap
                    (
                        $result,
                        array(
                            ConstFileCall::TAB_CALLABLE_PARAM_ARG_KEY_FILE_PATH => $strFilePath,
                            ConstFileCall::TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT => $tabArg
                        ),
                        $boolCallBefore,
                        $boolCallAfter
                    );
                }
            }

            // Throw exception, if callable not gettable
            if(is_null($result))
            {
                throw new CallConfigInvalidFormatException(serialize($this->getTabCallConfig()));
            }
        }

        // Return result
        return $result;
    }



}