<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\file\library;



class ConstFileCall
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN = 'file_path_format_pattern';
    const TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE = 'file_path_format_require';

    // Destination configuration
    const TAB_CALL_CONFIG_KEY_FILE_PATH_PATTERN = 'file_path_pattern';
    const TAB_CALL_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE = 'file_path_format_require';

    const CONF_CALL_PATH_SEPARATOR = ':';

    // Callable parameters configuration
    const TAB_CALLABLE_PARAM_ARG_KEY_FILE_PATH = 'file_path';
    const TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT = 'argument';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the file call configuration standard.';
    const EXCEPT_MSG_CALL_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the file call destination configuration standard.';
}