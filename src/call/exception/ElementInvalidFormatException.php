<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\exception;

use liberty_code\call\call\library\ConstCall;



class ElementInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $elm
     */
	public function __construct($elm)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCall::EXCEPT_MSG_ELEMENT_INVALID_FORMAT,
            mb_strimwidth(strval($elm), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
	 * Check if specified elements has valid format.
	 * 
     * @param mixed $elm
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($elm)
    {
        // Init string index array check function
        $checkTabStrIsValid = function($tabStr)
        {
            $result = is_array($tabStr);

            // Check each column name is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    is_string($strValue) ||
                    is_numeric($strValue) ||
                    is_bool($strValue) ||
                    is_null($strValue)
                );
            }

            return $result;
        };

		// Init var
		$result =
            // Check valid array
            is_array($elm) &&

            // Check valid config
            $checkTabStrIsValid($elm);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($elm) ? serialize($elm) : $elm));
		}
		
		// Return result
		return $result;
    }
	
	
	
}