<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\exception;

use liberty_code\call\call\library\ConstCall;



class CallableInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $callable
     */
	public function __construct($callable)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
            ConstCall::EXCEPT_MSG_CALLABLE_INVALID_FORMAT,
            mb_strimwidth(strval($callable), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified callable has valid format.
	 * 
     * @param mixed $callable
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($callable)
    {
		// Init var
		$result =
            is_null($callable) || // Check is null
            is_callable($callable); // Check is callable
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($callable);
		}
		
		// Return result
		return $result;
    }
	
	
	
}