<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\exception;

use liberty_code\call\call\library\ConstCall;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCall::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid element replace type
            (
                (!isset($config[ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE])) ||
                (
                    ($config[ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE] == ConstCall::ELM_REPLACE_TYPE_CONFIG_INDEX) ||
                    ($config[ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE] == ConstCall::ELM_REPLACE_TYPE_CONFIG_KEY_VALUE)
                )
            ) &&

            // Check valid element key pattern
            (
                (!isset($config[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN])) ||
                (
                    is_string($config[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN]) &&
                    (trim($config[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid element value pattern
            (
                (!isset($config[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN])) ||
                (
                    is_string($config[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN]) &&
                    (trim($config[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN]) != '')
                )
            ) &&

            // Check minimum requirement
            (
                (
                    isset($config[ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE]) &&
                    ($config[ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE] == ConstCall::ELM_REPLACE_TYPE_CONFIG_KEY_VALUE)
                ) ||
                (
                    (!isset($config[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN])) &&
                    (!isset($config[ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN]))
                )
            );

        // Return result
        return $result;
    }



    /**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}