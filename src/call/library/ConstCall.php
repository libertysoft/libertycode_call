<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\library;



class ConstCall
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CALL_CONFIG = 'tabCallConfig';
    const DATA_KEY_DEFAULT_CALLABLE_BEFORE = 'callableBefore';
    const DATA_KEY_DEFAULT_CALLABLE_AFTER = 'callableAfter';
	
	
	
	// Configuration
    const TAB_CONFIG_KEY_ELM_REPLACE_TYPE = 'elm_replace_type';
    const TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN = 'elm_format_key_pattern';
    const TAB_CONFIG_KEY_ELM_FORMAT_VALUE_PATTERN = 'elm_format_value_pattern';

    // Elements replace type configuration
    const ELM_REPLACE_TYPE_CONFIG_INDEX = 'index';
    const ELM_REPLACE_TYPE_CONFIG_KEY_VALUE = 'key_value';

    // Callable parameters configuration
    const TAB_CALLABLE_PARAM_ARG_KEY_RESULT = 'result';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default call configuration standard.';
    const EXCEPT_MSG_CALL_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default call destination configuration standard.';
    const EXCEPT_MSG_CALLABLE_INVALID_FORMAT = 'Following callable "%1$s" invalid! The callable must be null or a callback function.';
    const EXCEPT_MSG_ELEMENT_INVALID_FORMAT = 'Following array of elements "%1$s" invalid! It must be an array of stringable elements.';
}