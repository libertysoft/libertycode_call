<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\call\call\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxCall extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************
	
	/**
     * Split path and function from specified configuration.
     *
     * @param string $strConfig
	 * @param string $strSeparator
     * @param string &$strPath: Set null if not found, string else.
     * @param string &$strFunctionName: Set null if not found, string else.
     */
    public static function setConfigPathSeparation($strConfig, $strSeparator, &$strPath, &$strFunctionName)
    {
        // Init var
        $strPath = null;
        $strFunctionName = null;

        // Check config is valid
        if(
            is_string($strConfig) &&
            (trim($strConfig) != '')
        )
        {
            // Get info
            $tabStr = explode($strSeparator, $strConfig);

            // Check parts found
            if(
                (count($tabStr) == 2) && 
                (trim($tabStr[0]) != '') && // Check path valid
                (trim($tabStr[1]) != '') // Check function name valid
            )
            {
                $strPath = $tabStr[0];
                $strFunctionName = $tabStr[1];
            }
        }
    }
	
	
	
}