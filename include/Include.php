<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/call/library/ConstCall.php');
include($strRootPath . '/src/call/library/ToolBoxCall.php');
include($strRootPath . '/src/call/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/call/exception/CallConfigInvalidFormatException.php');
include($strRootPath . '/src/call/exception/CallableInvalidFormatException.php');
include($strRootPath . '/src/call/exception/ElementInvalidFormatException.php');
include($strRootPath . '/src/call/api/CallInterface.php');
include($strRootPath . '/src/call/model/DefaultCall.php');

include($strRootPath . '/src/call/di/library/ConstDiCall.php');
include($strRootPath . '/src/call/di/exception/ProviderInvalidFormatException.php');
include($strRootPath . '/src/call/di/model/DiCall.php');

include($strRootPath . '/src/call/di/clss/library/ConstClassCall.php');
include($strRootPath . '/src/call/di/clss/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/call/di/clss/exception/CallConfigInvalidFormatException.php');
include($strRootPath . '/src/call/di/clss/model/ClassCall.php');

include($strRootPath . '/src/call/di/dependency/library/ConstDependencyCall.php');
include($strRootPath . '/src/call/di/dependency/exception/CallConfigInvalidFormatException.php');
include($strRootPath . '/src/call/di/dependency/model/DependencyCall.php');

include($strRootPath . '/src/call/di/func/library/ConstFunctionCall.php');
include($strRootPath . '/src/call/di/func/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/call/di/func/exception/CallConfigInvalidFormatException.php');
include($strRootPath . '/src/call/di/func/model/FunctionCall.php');

include($strRootPath . '/src/call/file/library/ConstFileCall.php');
include($strRootPath . '/src/call/file/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/call/file/exception/CallConfigInvalidFormatException.php');
include($strRootPath . '/src/call/file/model/FileCall.php');

include($strRootPath . '/src/call/factory/library/ConstCallFactory.php');
include($strRootPath . '/src/call/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/call/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/call/factory/exception/CallConfigInvalidFormatException.php');
include($strRootPath . '/src/call/factory/api/CallFactoryInterface.php');
include($strRootPath . '/src/call/factory/model/DefaultCallFactory.php');

include($strRootPath . '/src/call/factory/file/library/ConstFileCallFactory.php');
include($strRootPath . '/src/call/factory/file/model/FileCallFactory.php');

include($strRootPath . '/src/call/factory/di/library/ConstDiCallFactory.php');
include($strRootPath . '/src/call/factory/di/model/DiCallFactory.php');

include($strRootPath . '/src/select/library/ConstSelector.php');
include($strRootPath . '/src/select/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/select/exception/CallInvalidFormatException.php');
include($strRootPath . '/src/select/exception/CallEnableGetException.php');
include($strRootPath . '/src/select/api/SelectorInterface.php');
include($strRootPath . '/src/select/model/DefaultSelector.php');