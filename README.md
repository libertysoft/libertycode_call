LibertyCode_Call
================



Description
-----------

Library contains callable components, 
to manage functions and/or methods calls.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/call ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/call": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Call

Calls allows to get callback function, 
to execute specific destination, 
from specific destination configuration.

_Elements_

- Call

    Allows to design a call, 
    who is an item containing destination configuration array, 
    to get executable (callback function), 
    to execute specific destination.

- FileCall

    Extends call features. 
    Allows to configure file destination (php file inclusion).
    
- DiCall (extend HandleRoute)

    Extends call features. 
    Uses DI provider, 
    to get executable,
    to execute specific destination.
    
- ClssCall

    Extends DI call features. 
    Allows to configure class and method destination.
    
- DependencyCall

    Extends DI call features. 
    Allows to configure dependency and method destination.
    
- FunctionCall

    Extends DI call features. 
    Allows to configure function destination.

- CallFactory

    Allows to design a call factory, 
    to provide new or specified call instance,
    from specified destination configuration.
    
- FileCallFactory

    Extends call factory features. 
    Provides file call instance.
    
- DiCallFactory

    Extends call factory features. 
    Provides DI call instance.

_Example_

```php
// Get DI provider
use liberty_code\di\provider\model\DefaultProvider;
$provider = new DefaultProvider();
...
// Get class call
use liberty_code\call\call\di\clss\model\ClassCall;
$call = new ClassCall($provider);
...
// Set destination configuration
$call->setCallConfig(array(...));
...
// Get destination executable
$callable = $call->getCallable();
...
// Execute destination
$result = $callable();
echo($result); // Show destination callback return, null if no return found
...
// Get call factory
use liberty_code\call\call\factory\di\model\DiCallFactory;
$callFactory = new DiCallFactory($provider);
...
// Get new call from destination configuration
$call = $callFactory->getObjCall( array(...));
...
```

#### Selector

Selector allows to select and provide a specific call object, 
from a specified destination configuration.

_Elements_

- Selector
    
    Allows to select and provide a specific call object,
    from a set of predefined call objects,
    and a specified destination configuration.
    
_Example_

```php
// Get predefined call objects
use liberty_code\call\call\file\model\FileCall;
use liberty_code\call\call\di\clss\model\ClassCall;
use liberty_code\call\call\di\dependency\model\DependencyCall;;
use liberty_code\call\call\di\func\model\FunctionCall;
$tabCall = array(
    new FileCall(),
    new ClassCall($provider),
    new DependencyCall($provider),
    new FunctionCall($provider)
);
...
// Get selector
use liberty_code\call\select\model\DefaultSelector;
$selector = new DefaultSelector($callFactory, $tabCall);
...
// Get call, from and hydrated with specified destination configuration
$call = $selector->getObjCall(array(...));
...
```

---


